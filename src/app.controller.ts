import { Controller, Get, Post, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { AppService } from './app.service';
import { helper } from './shared/helper';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @UseInterceptors(
    FileInterceptor(
      'file',
      {
        storage : diskStorage({
          destination: helper.destinationPath,
          filename: helper.customFileName,
          
        })
      }
    )
  )

  @Post('file')
  uploadFile(@UploadedFile() file : Express.Multer.File) {
    return {
      msg: `The file ${file.originalname} has been saved on the server as ${file.filename}`
    };
  }
}

/*
Possible code for the front:

var bodyFormData = new FormData();
const imageFile = "/Users/joseavila.nerio/Desktop/catedral.culiacan.sinaloa.jpeg";
bodyFormData.append('file', imageFile); 

axios({
  method: 'post',
  url: 'http://localhost:3001/file',
  data: bodyFormData,
  headers: {
     'Content-Type': 'multipart/form-data'
  }
})
.then(function(response) {
  //handle success
  console.log(response);
})
.catch(function(response) {
  //handle error
  console.log(response);
});

*/
