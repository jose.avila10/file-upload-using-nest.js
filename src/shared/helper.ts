import { BadRequestException } from "@nestjs/common";

//To help calculus of max sizes
const KB = Math.pow(1024, 1);
const MB = Math.pow(1024, 2);
const GB = Math.pow(1024, 3);
const TB = Math.pow(1024, 4);

const maxFileSize = 100 * KB;
const allowedExt = ["jpeg", "jpg"];

export class helper {
    static customFileName(req, file, cb) {
        var file_ext = file.originalname.substr(file.originalname.lastIndexOf(".") + 1);

        var fileCategory = "image"; //Do not use spaces
        var randomId = Math.floor(Math.random() * (1000000 - 1)) + 1; // Math.floor(Math.random() * (max - min)) + min;

        var newFileName = fileCategory + "_" + Date.now() + "-" + randomId + "." + file_ext;

        cb(null, newFileName);
    }

    static destinationPath(req, file, cb) {
        var file_ext = file.originalname.substr(file.originalname.lastIndexOf(".") + 1);
        const found = allowedExt.find(ext => ext === file_ext);
        if(found === undefined) return cb(new BadRequestException('File extension not allowed'))

        const fileSize = req.headers['content-length'];
        if( fileSize > maxFileSize) return cb(new BadRequestException('Too large file'))

        cb(null, './uploads/')
    }
}